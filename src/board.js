function Board(width,height){
    this.cw = width; // canvas width
    this.ch = height // canvas height

    this.tiles = new Array();
    this.houses = new Array();
    this.volcanos = new Array();
    this.treadmills = new Array();

    this.zombies = new Array();

    this.noise = 0.1;


    this.treadmillReward = -1.0;
    this.volcanoReward = 50.0;
    this.houseReward = -50.0;
}



Board.prototype.destroy = function(){
    this.tiles.length = 0;

    var i;
    for(i =0; i < this.houses.length; i++){
	this.houses.sprite.kill();
    }
    this.houses.length = 0;

    for(i = 0; i < this.volcanos.length; i++){
	this.volcanos.sprite.kill();
    }
    this.volcanos.length = 0;

    for(i = 0; i < this.treadmills.length; i++){
	this.treadmills.sprite.kill();
    }
    this.treadmills.length = 0;
}



Board.prototype.create = function(tilesX,tilesY){
    this.destroy();

    this.tilesX = tilesX;
    this.tilesY = tilesY;
    this.numTiles = this.tilesX*this.tilesY;

    this.tw = this.cw/this.tilesX; // tile width
    this.th = this.ch/this.tilesY; // tile height

    this.sx = this.tw/100; // scale x for sprites
    this.sy = this.th/100; // scale y for sprites

    var i,j;
    var index = 0;
    for(i = 0; i < this.tilesY; i++){
        for(j = 0; j < this.tilesX; j++){
            this.tiles.push(0);

	    var y = i*this.th;
            var x = j*this.tw;
            var v = new Volcano(x,y,this.sx,this.sy,index,this);
            var h = new House(x,y,this.sx,this.sy,index,this);
            var t = new Treadmill(x,y,this.sx,this.sy,
                                  Math.floor(Math.random()*4),
                                  index,this);
            v.sprite.nextSprite = h.sprite;
            h.sprite.nextSprite = t.sprite;
            t.sprite.nextSprite = v.sprite;
            this.volcanos.push(v);
            this.houses.push(h);
            this.treadmills.push(t);

            index++;
	}
    }
}



Board.prototype.randomize = function(){
    var tiles = new Array();
    var arrows = new Array();
    var i;
    for(i = 0; i < this.numTiles; i++){
	tiles.push(Math.floor(Math.random()*3.0));
	arrows.push(Math.floor(Math.random()*4.0));
    }

    // set board randomizes arrows if they're not passed explicitly
    this.setBoard(tiles);
}


Board.prototype.setBoard = function(tiles,arrows){
    var i;

    if(arrows == undefined){
	arrows = new Array();
	for(i = 0; i < this.numTiles; i++){
	    arrows.push(Math.floor(Math.random()*4.0));
	}
    }

    for(i = 0; i < this.numTiles; i++){
	this.tiles[i] = tiles[i];
	if(tiles[i] == 0){
	    this.volcanos[i].sprite.visible=true;
	    this.houses[i].sprite.visible=false;
	    this.treadmills[i].sprite.visible=false;
	}
	else if(tiles[i] == 1){
	    this.volcanos[i].sprite.visible=false;
	    this.houses[i].sprite.visible=true;
	    this.treadmills[i].sprite.visible=false;
	}
	else{
	    this.volcanos[i].sprite.visible=false;
	    this.houses[i].sprite.visible=false;
	    this.treadmills[i].sprite.visible=true;
	}

	this.treadmills[i].sprite.frame = arrows[i];
    }
}


Board.prototype.killZombies = function(){
    var i = 0;
    for(i = 0; i < this.zombies.length; i++){
	this.zombies[i].sprite.kill();
    }
    this.zombies.length = 0;
}



Board.prototype.dropZombies = function(){
    this.killZombies();

    var i,j,k;
    for(i = 0,k = 0; i < this.tilesY; i++){
	for(j = 0; j < this.tilesX; j++,k++){
	    if(this.tiles[k]==2){
		var y = i*this.th;
		var x = j*this.tw;
		this.zombies.push(new Zombie(k,x,y,this.sx,this.sy));
	    }
	}
    }
}


Board.prototype.puffVolcanos = function(){
    var i;
    for(i = 0; i < this.volcanos.length; i++){
	var v = this.volcanos[i];
	if(v.sprite.visible && !v.burn.isPlaying){
	    v.sprite.animations.play("puff");
	}
    }
}


Board.prototype.moveZombies = function(){
    var i;
    for(i = 0; i < this.zombies.length; i++){
        var z = this.zombies[i];
        if(this.treadmills[z.tile].sprite.visible && !z.tween.isRunning){
	    if(this.tiles[z.tile] == 2){
		var frame = this.treadmills[z.tile].sprite.frame
		if(Math.random() < this.noise){
		    frame = Math.floor(Math.random()*4)
		}
		var dx = 0;
		var dy = 0;
		var newTile = this.calcTile(z.tile,frame);
		if(newTile != z.tile){
		    if(frame == 0){
			dx = 100;
		    }
		    else if(frame == 1){
			dx = -100;
		    }
		    else if(frame == 2){
			dy = -100;

		    }
		    else{
			dy = 100;
		    }
		}

		z.tile = newTile;

		z.tween = game.add.tween(z.sprite);

		var j;
		for(j = 0; j < 11; j++){
		    if(dx != 0 || dy != 0){
			z.tween.to({x:z.sprite.x+(j+1.0)*dx/11.0,
				    y:z.sprite.y+(j+1.0)*dy/11.0,
				    frame:j%5},
				   1,null,false,2000.0/11.0);
		    }
		    else{
			z.tween.to({x:z.sprite.x,
				    y:z.sprite.y,
				    frame:0},
				   1,null,false,2000.0/11.0);
		    }
		}
		z.tween.start();
	    }
	}
	else if(this.houses[z.tile].sprite.visible && !z.tween.isRunning
		&& z.sprite.visible){
	    z.sprite.visible = false;
	    this.houses[z.tile].sprite.animations.play("invade");
	}
	else if(this.volcanos[z.tile].sprite.visible && !z.tween.isRunning
		&& z.sprite.visible){
	    z.sprite.visible = false;
	    this.volcanos[z.tile].sprite.animations.stop();
	    this.volcanos[z.tile].sprite.animations.play("burn");
	}
    }
}



Board.prototype.transProb = function(from,arrow,to){
    if(this.tiles[from] != 2){
	return 0.0;
    }
    else{
	var prob = 0.0;
	var next;

	// no adherence to arrow
	next = this.calcTile(from,arrow);
	if(next == to){
	    prob += 1.0 - this.noise;
	}

	// no adherence to arrow
	for(var a = 0; a < 4; a++){
	    next = this.calcTile(from,a);
	    if(next == to){
		prob += this.noise/4.0;
	    }
	}
	return prob;
    }
}



Board.prototype.calcTile = function(tile,arrow){
    if(arrow == 0){
	if((tile+1) % this.tilesX == 0)
	    return tile;
	else
	    return tile+1;
    }
    else if(arrow == 1){
	if(tile % this.tilesX == 0)
	    return tile;
	else
	    return tile-1;
    }
    else if(arrow == 2){
	if(tile < this.tilesX)
	    return tile;
	else
	    return tile - this.tilesX;
    }
    else if(arrow == 3){
	if((this.numTiles - tile) <= this.tilesX)
	    return tile;
	else
	    return tile + this.tilesX;
    }
    else{
	throw "arrow is not in {0,1,2,3}"
    }
}


Board.prototype.expReward = function(tile,arrow){
    if(this.tiles[tile] != 2)
	return 0.0;
    else{
	var reward = 0.0;
	for(var i = 0; i < this.numTiles; i++){
	    var r;
	    if(this.tiles[i] == 0)
		r = this.volcanoReward;
	    else if(this.tiles[i] == 1)
		r = this.houseReward;
	    else if(this.tiles[i] == 2)
		r = this.treadmillReward;

	    reward += r * this.transProb(tile,arrow,i);
	}
    }
    return reward;
}


Board.prototype.improvePolicy = function(){
    update = policyUpdate(this,0.9);
    if(update.change){
	var i;
	for(i = 0; i < this.numTiles; i++){
	    this.treadmills[i].sprite.frame = update.arrows[i];
	}
    }
    else{
	alert("Policy is optimal.")
    }
}
