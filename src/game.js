var game = new Phaser.Game(500, 500, Phaser.AUTO, "zombie-game",
			   { preload: preload, create: create,
			     update: update });


function preload() {
    game.load.spritesheet("house","images/house.png",100,100)
    game.load.spritesheet("treadmill","images/treadmill.png",100,100)
    game.load.spritesheet("volcano","images/volcano.png",100,100)
    game.load.spritesheet("zombie","images/zombie.png",100,100)
}

function create() {
    board = new Board(game.width,game.height);
    board.create(5,5);
    board.randomize();
}

function update() {
    board.moveZombies();
    board.puffVolcanos();
}
