function Zombie(tile,x,y,sx,sy){
    this.tile = tile;

    this.sprite = game.add.sprite(x,y,"zombie");
    this.sprite.animations.add("walk",[0,1,2,3,4],11.0/2.0,true);

    this.sprite.scale.set(sx,sy);
    this.sprite.visible=true;

    this.tween = game.add.tween(this.sprite);
}
