function Volcano(x,y,sx,sy,tileIndex,board){
    this.sprite = game.add.sprite(x,y,"volcano");
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputDown.add(switchTile,this);

    var frameOrder = new Array();
    var start = Math.floor(Math.random() * 3.0);
    var i;
    for(i = 0; i < 3; i++){
	frameOrder.push((start+i)%3);
    }
    this.sprite.animations.add("puff",frameOrder,5,true);
    this.burn = this.sprite.animations.add("burn",[3,4,6,5,7,8],3,false);

    this.sprite.scale.set(sx,sy);
    this.sprite.visible=false;

    this.sprite.nextSprite = null;
    this.sprite.board = board;
    this.sprite.tileIndex = tileIndex;
    this.sprite.tileValue = 0;
}


function House(x,y,sx,sy,tileIndex,board){
    this.sprite = game.add.sprite(x,y,"house");
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputDown.add(switchTile,this);

    this.sprite.animations.add("invade",[1,2,3,4,5,6,7,8,9,0],5,false);

    this.sprite.scale.set(sx,sy);
    this.sprite.visible=false;

    this.sprite.nextSprite = null;
    this.sprite.board = board;
    this.sprite.tileIndex = tileIndex;
    this.sprite.tileValue = 1;
}


function Treadmill(x,y,sx,sy,position,tileIndex,board){
    this.sprite = game.add.sprite(x,y,"treadmill");
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputDown.add(rotateTreadmill,this);
    this.sprite.events.onInputDown.add(switchTile,this);

    this.sprite.scale.set(sx,sy);
    this.sprite.frame = position;
    this.sprite.visible = false;

    this.sprite.nextSprite = null;
    this.sprite.board = board;
    this.sprite.tileIndex = tileIndex;
    this.sprite.tileValue = 2;
}

var switchTile = function(sprite,pointer) {
    if(sprite.visible && pointer.rightButton.isDown) {
        Debugger.log(sprite);
        sprite.visible = false;
        sprite.nextSprite.visible = true;
        sprite.board.tiles[sprite.tileIndex] = sprite.nextSprite.tileValue;
    }
}

var rotateTreadmill = function(sprite,pointer){
    if(sprite.visible && pointer.leftButton.isDown){
	if(sprite.frame == 2){
	    sprite.frame = 0;
	}
	else if(sprite.frame == 3){
	    sprite.frame = 1;
	}
	else if(sprite.frame == 1){
	    sprite.frame = 2;
	}
	else{
	    sprite.frame = 3;
	}
    }
}
