function getValue(board,gamma,arrows){
    Debugger.log("Get value");
    if(arrows == undefined){
	Debugger.log("Use arrows on board");
	var arrows = new Array();
	for(var i = 0; i < board.numTiles; i++){
	    arrows.push(board.treadmills[i].sprite.frame);
	}
    }
    else{
	Debugger.log("Arrows are supplied");
	if(arrows.length != board.numTiles)
	    Debugger.log("Arrows supplied is not correct length");
    }

    var I = numeric.identity(board.numTiles);

    var P = [];
    for(var i = 0; i < board.numTiles; i++){
	var Pi = [];
	for(var j = 0; j < board.numTiles; j++){
	    Pi.push(board.transProb(i,arrows[i],j));
	}
	P.push(Pi);
    }

    var R = [];
    for(var i = 0; i < board.numTiles; i++){
	R.push(board.expReward(i,arrows[i]));
    }

    var V = numeric.solve(numeric.sub(I,numeric.mul(gamma,P)),R);

    // check if valid solution
    var valid = true;
    for(var i = 0; i < V.length; i++){
	if(!isFinite(V[i]))
	    valid = false;
    }

    if(!valid){
	// if not valid, alert the user and then return vector of zeros
	var msg = "V is invalid.  Likely using a discount factor "
	    + "of 1.0 and zero noise.  Try adding noise or "
	    + "decreasing the discount factor.";
	Debugger.log(msg);
	alert(msg);
	var zeros = new Array();
	for(var i = 0; i < board.numTiles; i++){
	    zeros.push(0.0);
	}
	return zeros;
    }
    else{
	// if valid return the solution
	return V;
    }
}


function policyUpdate(board,gamma,arrows){
    // updates in place
    Debugger.log("Update policy");
    if(arrows == undefined){
	Debugger.log("Use arrows on board");
	var arrows = new Array();
	for(var i = 0; i < board.numTiles; i++){
	    arrows.push(board.treadmills[i].sprite.frame);
	}
    }
    else{
	Debugger.log("Arrows are supplied");
	if(arrows.length != board.numTiles)
	    Debugger.log("Arrows supplied is not correct length");
    }

    var V = getValue(board,gamma,arrows);

    var change = false;
    for(var i = 0; i < board.numTiles; i++){
	var vMax = -Infinity
	var aMax = undefined
	for(var a = 0; a < 4; a++){
	    var vA = 0.0
	    for(var j = 0; j < board.numTiles; j++){
		vA += V[j] * board.transProb(i,a,j);
	    }
	    vA += board.expReward(i,a);
	    if(vA > vMax){
		aMax = a;
		vMax = vA;
	    }
	}

	if(arrows[i] != aMax && board.tiles[i] == 2){
	    change = true;
	}

	if(arrows[i] != aMax){
	    arrows[i] = aMax;
	}
    }
    return {change:change, arrows:arrows.slice(0)};
}
