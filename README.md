# Git Repo for Zombie and Treadmill Game
- `cd ZombieTreadmill/src`
- `python -m SimpleHTTPServer`
  - Output will show an ip and port
  - In a browser, navgiate to `ip:port` where ip and port are from the
    SimpleHTTPServer output
